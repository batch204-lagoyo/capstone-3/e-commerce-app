import './App.css';
import AppNavBar from './components/AppNavbar';
import { useState,useEffect } from 'react';
import { UserProvider } from './UserContext';
import { BrowserRouter as Router, Routes, Route, } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Home from './Pages/Home';
import Login from './Pages/Login';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })        
      }
    })
  }, [])

  return (
    <UserProvider value={{user, setUser}}>
    <Router>
   
    <AppNavBar/>
    <Container>
        <Routes>
          <Route  path="/" element={<Home/>}/>
          {/* <Route exact path="/courses" component={Courses}/>
          <Route exact path="/courses/:courseId" component={SpecificCourse}/>
          <Route exact path="/register" component={Register}/> */}
          <Route  path="/login" element={<Login/>}/>
          {/* <Route exact path="/logout" component={Logout}/> */}
          {/* <Route component={Error} /> */}
        </Routes>
    </Container>

</Router>
</UserProvider>
    

  );
}

export default App;
