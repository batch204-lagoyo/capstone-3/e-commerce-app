// import { useContext } from 'react';
import '../App.css';
import { useContext } from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import Navlogo from '../images/nav-logo.jpg'

export default function AppNavBar(){

	
	const { user } = useContext(UserContext);

	return(
		<Navbar bg="success" variant="dark" expand="lg">
		  <Container>
		    <Link className="navbar-brand " to="/"> <img src={Navlogo} height="50px"/> ZCPC Hive </Link>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
			
		      <Nav className="ms-auto" >
		        <Link className="nav-link" to="/">Home</Link>
		        <Link className="nav-link" to="/courses" exact>Products</Link>

		        {(user.id !== null) ?
		        	<Link className="nav-link" to="/logout">Logout</Link>
		        	 : 
		        	<>
			        	<Link className="nav-link" to="/login">Login</Link>
			        	<Link className="nav-link" to="/register">Register</Link>
		        	</>
		}

		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}
