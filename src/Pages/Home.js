import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){

	const data = {
		title: "ZCPC Hive",
		content: "Quality Products, Price Guaranteed",
		destination: "/login",
		label: "Purchase now!"
	}


	return(
		<>
			<Banner dataProp={data}/>
            <Highlights />
		</>
	)
}
